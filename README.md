## Cloud Firewall

***Note: This addon is an educational research tool to replicate Kashmir Hill's research on living without tech giants. I have removed the listing from Mozilla addons store as I no longer have the time to support it with updates/fixes/features. 
Since the intended users of this addon are technical people who wished to replicate her research, they can build this addon from source and run it in a temporary profile in Firefox. Refer web-ext documentation in Mozilla regarding running it in temporary profile (web-ext run)***

Supported browsers : [Firefox from Mozilla.org](https://mozilla.org), Mozilla's "Firefox for Android" via Play Store. A Cloud Firewall user helped test this addon in Firefox forks like Fennec, IceCatMobile from F-droid.org store in /e/ OS, Lineage OS and Fairphone FP OS.
NOT supported for Firefox Variants, like TBB, Waterfox, Palemoon etc., also not for use with Tor Browser or proxy addons

**This project is no longer maintained. No support will be provided**

### What is Cloud Firewall? <a class="no-attachment-icon" href="https://github.com/humanetech-community/awesome-humane-tech#privacy" target="_blank"><img src="https://gitlab.com/gkrishnaks/cloud-firewall/raw/master/src/images/humanetech.svg" alt="Badge for Humane Technology listing"></a>

-   Cloud Firewall is a browser extension/addon that allows users to block connections to sites, pages and web resources (images, videos, etc) hosted in major cloud services if the user wishes to do so.

    _Example 1:_ If you enable "Block Amazon" in Popup, all websites hosted on Amazon AWS will be blocked. You can completely disable your chosen rules for any site you wish, from popup menu.

    _Example 2:_ If you enable "Block Amazon" in Popup, let's say a website is hosted "on-premise (not in cloud)", but has some "resources" like images, video and scripts hosted on Amazon AWS cloud, only those "3rd party" resources are blocked. "3rd party" = these resources. "1st party" = site in address bar + resources delivered from same domain on-premise server.

-   Regarding Toggle switches in popup menu, the default upon install is "Allow all supported tech companies/clouds". Any allow/block toggling is not persisted across browser restarts i.e. whenever the browser is restarted, all switches are toggled OFF = allow top companies/clouds when browser launches. For advanced users : You have the option to persist the allow/block rules using the switch in Settings page.

### Inspiration:

-   The inspiration behind the creation of this addon was the "Life without the tech giants" series by Kashmir Hill and Dhruv Mehrotra. Read it here : [Gizmodo article series](https://gizmodo.com/life-without-the-tech-giants-1830258056) and Dhruv Mehrotra published the VPN tool in his [Github Repository](https://github.com/dmehrotra/GoodbyeBigFive) .
-   I used the AS.csv from his repository and the BASH script to generate the list of IPv4/IPv6 CIDR blocks/ranges owned by the 5 companies (Google, Facebook, Amazon, Microsoft, Apple). Cloudflare IPv4/IPv6 list is taken from public information at https://<i></i>cloudflare.com/ips/

### Purpose:

-   **Experience** the web as though major cloud companies are not available, so that we can visualize how much of the WWW is hosted in some cloud providers
-   **Research:** similar to Kashmir Hill's series, you can conduct research on how many times your browser connects to major top tech cloud companies when you browse the WWW. (Please let the developer know if this addon helps with your research)
-   **Website Testing:** If you are a website developer or site owner, and have some resources like javascript or images or videos hosted in Cloud providers like AWS or Azure or Google Cloud, use this addon for testing your website behaviour for the case where the whole Cloud region goes down. (Please let the developer know if this addon helps with testing your websites)

### How does it work?

-   Addon comes bundled with a list of all IP address ranges CIDR blocks in both IPV4 and IPV6 owned by top cloud tech companies. Refer "Inspiration" section above for the source of this list.
-   Whenever a page is loaded, all the resources in the page from address bar URL to embedded images, javascript files and other resources (like video) are resolved from DNS to IP address, and the IP address is checked against the known list of IP ranges owned by each company
-   If "Block {company}" switch is toggled ON in popup, the connection is blocked if the resolved IP is within a known range of the selected company.
-   Addon uses local offline storage to store a cache of hostnames so that subsqeuent page loads are faster. Domain names are stored as base64 SHA256 digest sums. User has the option to delete this cache OR disable cache storage from Settings page
-   In future versions, we will research an optional setting to AES256 encrypt this cache
-   When caching is enabled, cache is compressed and written to disk once every four minutes to avoid multiple disk writes
-   **\*NOTE**: it uses the [DNS.resolve WebExt API](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/dns/resolve) made available by Firefox to WebExtensions. The implementation of this API is inside browser, and browser uses SYSTEM's DNS resolver to resolve DNS to IP. Therefore only the official Firefox versions from Mozilla are supported, namely "Firefox" from [mozilla.org](https://mozilla.org) or their "Firefox For Android" from Play store.  
    Hence it's **not recommended** to use this addon with other Firefox variants, like TBB, Palemoon, Waterfox etc. Also **not recommended** to use with any "proxy addons" you may find in Mozilla Addons store. Please see Issue [#18](https://gitlab.com/gkrishnaks/cloud-firewall/issues/18) if you wish to help change this

### Stats in Popup Menu

-   Stats are displayed in popup menu. Stats include the number of connections blocked by the addon per company, and total connections blocked count across all.
-   Since the stat counts are per-device (per-install) statistics, it is not included in Export/Import setting feature. (Export/Import settings option will be added in a future version)
-   To avoid multiple disk writes, stats are stored to disk once every four minutes. It's fine if browser is closed before a four minute window as stats are not a critical value :)

### Credits and Acknowledgements:

-   Refer Inspiration section above. This addon uses the AS.csv and bash script from this Github Repo : [Github page](https://github.com/dmehrotra/GoodbyeBigFive)

-   _This addon uses the following libraries for UI :_  
     Bootstrap (CSS and JS)  
     FontAwesome  
     JQuery.  
     CSS for "toggle switches" was taken from this article in blog : [Dev.to post](https://dev.to/link2twenty/accessibility-first-toggle-switches-3obj)  
     These are governed by their respective licenses. You can refer to their websites for the same.

-   _This addon uses the following libraries for functionality/utilities :_  
     LZString data compression : [Pieroxy.net](http://pieroxy.net/blog/pages/lz-string/guide.html)  
     PublicSuffixList by Mozilla: [Public Suffix List](https://publicsuffix.org/)  
     UriTools.js from UblockOrigin: [Repository](https://github.com/gorhill/uBlock/blob/master/src/js/uritools.js)  
     ipaddr.js : [Repository](https://github.com/whitequark/ipaddr.js)  
     ip-range-check package : [Repository](https://github.com/danielcompton/ip-range-check)  
     Forge library for SHA256 digest hashes: [Repository](https://github.com/digitalbazaar/forge)  
     Punycode.js : [Repository](https://github.com/bestiejs/punycode.js/)  
     tomlify-j0.4: [Repository](https://github.com/jakwings/tomlify-j0.4)  
     json2yaml.js: [Repository](https://github.com/jeffsu/json2yaml)  
     CSON parser: [Repository](https://github.com/groupon/cson-parser)  
     json2plain.js: [Repository](https://github.com/jarradseers/json2plain)
    These are governed by their respective licenses. You can refer to their websites for the same.

-   This addon uses the icons created by Isaac Grant and he published it under Creative Commons license : [IconFinder page](https://www.iconfinder.com/icons/2739111/protection_secure_shield_icon). It also uses the cloud icon from [Iconfinder page](https://www.iconfinder.com/icons/316014/cloud_icon) by Yannick Lung which he released under "Free for commerical use" license.

### License

The Cloud Firewall addon is licensed under GPLv3 license. Refer the full license here : [License](LICENSE)

\---

Cloud Firewall - a browser extension/addon that allows users to block connections to sites, pages and web resources (images, videos, etc) hosted in major cloud services if the user wishes to do so.
Copyright (C) 2019 Gokulakrishna Sudharsan

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/

\---

Privacy Policy in one line : We do not collect your data. Detailed privacy policy page : [Privacy Policy](Privacy%20Policy)

**This project is no longer maintained. No support will be provided**